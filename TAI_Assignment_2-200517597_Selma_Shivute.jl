import Pkg; Pkg.add("MLJDecisionTreeInterface")

using CSV, DataFrames, Distances, StatsBase, Clustering, Random, StatsPlots, LinearAlgebra
using DecisionTree, Random

#using MLJ
using Markdown
using InteractiveUtils
using PlutoUI;PlutoUI.TableOfContents()

# Load the data into a dataframe called wine_df
wine_df=CSV.read("C:/Users/Selma Shivute/Documents/NUST/Year 2/Semester 1/AI/dataset/wine-clustering.csv", DataFrame)

#DATA EXPLORATORY 

schema(wine_df)

y, X = unpack(wine_df, ==(:MedV))

X1 = coerce(X, autotype(X, rules=(:discrete_to_continuous,)))





#summary statistics of a column in a DataFrame, including the count of missing values.
describe(wine_df)



#finding uniquiness of the column
unique(wine_df[!,:Alcohol])

#checking for missing values and found none
ismissing.(wine_df)

#= Checking the skewness of our dataset.
A normally distribuited data has a skewness close to zero.
Skewness greather than zero means that there is more weight in the left side of the data.
In another hand, skewness smaller than 0 means that there is more weight in the right side of the data=#

skewness(Matrix(wine_df))

# Normalize the data
#df_normalized = normalize(wine_df, dims=1)

using LinearAlgebra

function normalize(wine_df)
   df_norma = norm(wine_df)
    if df_norma == 0
        return wine_df
    else
        return wine_df / df_norma
    end
end


#DATA PREPROCESSING 

function scale_and_copy(data::DataFrame)
    eval(Meta.parse("import ScikitLearn: StandardScaler, fit_transform!"))
    std_scaler = StandardScaler()
    scaled_data = copy(wine_df)
    scaled_data[:, :] = fit_transform!(std_scaler, scaled_data)
    return scaled_data
    
    scaled_wine_cluster = scale_and_copy(wine_df)
end



# Generate  matrix
X = Matrix(wine_df)

# Calculate mean and standard deviation of each column
μ = mean(X, dims=1)
σ = std(X, dims=1)

# Normalize matrix along columns
X_normalized = (X .- μ) ./ σ

# Convert back to DataFrame if necessary
df_normalized = DataFrame(X_normalized, names(wine_df))


describe(df_normalized)

#CLUSTERING

#Kmean clustering
using Clustering, DataFrames

df_scaled_matrix = Matrix(df_normalized)

kmeans_model = kmeans(df_scaled_matrix', 50)
centroids = kmeans_model.centers
labels_kmeans = assignments(kmeans_model)

scatter(wine_df.Alcohol, marker_z=kmeans_model.assignments,color=:lightrainbow, legend=:false, title=:"K-MEANS")

# Extract the features from the dataframe
features = Matrix(wine_df[:, 2:end])



Random.seed!(983)
n = 9

for i in 1:n
   last = minimum([i+Int(floor(n/4)), n])   
   for j in i:last
       features[i,j] = 1
   end
end

mat = features[:, randperm(n)]
heatmap(features)



# Calculate the pairwise distances between data points using Euclidean distance
dismat = pairwise(Euclidean(), mat, dims=2)
hcl = hclust(dismat, linkage=:ward) # ward linkage

#display the clustring
plot(hcl, xticks=(collect(1:n), ["$i" for i in hcl.order]), 
title=:"Wine linkage clustering of Wine Dataset")



theme(:ggplot2)
gr(size = (700, 700))
result = hclust(dismat, linkage=:complete)
plot(
    plot(result, xticks=true),title="Complete linkage Dendrogram and Heatmap",
    heatmap(mat[:, hcl.order], colorbar=true, 
xticks=(1:n, ["$i" for i in hcl.order])),
    layout=grid(2,1, heights=[0.2,0.8])
    )

theme(:bright)
gr(size = (700, 700))
    resultS = hclust(dismat, linkage=:single)
plot(
    plot(result, xticks=true),title="Single linkage Dendrogram",    
    layout=grid(1,1, heights=[0.2,0.8])
    )

heatmap(mat, xticks=true,title="Single linkage Heatmap")

theme(:bright)
gr(size = (700, 700))
scatter(mat, legend=:bottomleft, 
title=:"Hierarchical Clustering of Wine Dataset")

# the k-means was not part of the exersice but i did it for comparison purposes and it looks a bit better further up!
#below is the Hierarchical Clustering for dismat of complete denogram which look much better than the single linkage 

theme(:bright)
gr(size = (700, 700))
scatter(dismat, legend=:bottomleft, 
title=:"Hierarchical Clustering of Wine Dataset")

#Boosting Ensemble Technique

# Choose the number of clusters and height
num_clusters = 3
h=0.5

labels = cutree(hcl; h)

# Print the clusters
println(labels)

#I created a matrix from a choosen column in the dataset 
MatriFea= Matrix(wine_df[:, [3, 5, 6, 9, 10]])


features1 =Matrix(wine_df[:, 1:4])
labels1 = Vector(wine_df[:, 5])

#MODEL IMPLEMENTATION 

# Split the data into training and testing sets
train_features = features1[1:120, :]
train_labels = labels1[1:120]
test_features = features1[121:end, :]
test_labels = labels1[121:end]

#  decision tree model
ensemble = DecisionTreeClassifier(max_depth=2)
#fit the model with train data
DecisionTree.fit!(ensemble, train_features, train_labels)

example = [14,13, 72.5, 1.71, 4.1, 2.74]

# Make predictions on the test set
predictions = DecisionTree.predict(model, test_features)
#predictions = DecisionTree.predict(model, example)

# Evaluate the model
accuracy = mean(predictions .== test_labels)


